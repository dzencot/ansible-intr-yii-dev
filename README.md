# Ansible playbook for Introvert developers

Development environment deployment automation

### Prerequisities

- Unix OS (for Windows use Cygwin or virtualization)
- git
- ansible
- make
- npm
- eslint

### Installing

Clone this repo

### Usage

* Create new client:

    + ```
      make create_client client_name="test-client" clientName="testClient" d="./"
      ```
    + Добавить в массив `/common/config/clients-aliases.php` запись: `'@testClient' => 'test-client',`

    `testClient` - название клиента

    `test-client` - название клиента для создания путей, url

    `d=./` - путь где будет создана папка с клиентом(по умолчанию `./`)

* Запушить код из текущей ветки в dev ветку:

    + Запушить без проверки
      ```
      make push-dev m="save work"
      ```
    + Запушить с проверкой линтера(коммит не будет создан если есть ошибки)
      ```
      make linter-push m="save work"
      ```

