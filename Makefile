current_dir = $(shell pwd)
repo_dir = $(shell git rev-parse --git-dir | cut -f 1 -d '.')
mkfile_dir := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
# current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
d ?= ./
branch ?= $(shell git rev-parse --abbrev-ref HEAD)
m ?= 'save work'

create_client:
	ansible-playbook $(mkfile_dir)/create-client.yml -i localhost --connection=local -e clientName="$(clientName)" -e client_name="$(client_name)" -e dest="$(d)" -e current_dir="$(current_dir)" -v

push-dev:
	ansible-playbook $(mkfile_dir)/push.yml -i localhost --connection=local -e branch_name="$(branch)" -e commit_text="$(m)" -e repo_dir="$(repo_dir)" -e current_dir="$(current_dir)" -e linter="no" -e push_dev="yes" -v

linter:
	ansible-playbook $(mkfile_dir)/push.yml -i localhost --connection=local -e branch_name="$(branch)" -e commit_text="$(m)" -e repo_dir="$(repo_dir)" -e current_dir="$(current_dir)" -e linter="yes" -e push_dev="no"

linter-push:
	ansible-playbook $(mkfile_dir)/push.yml -i localhost --connection=local -e branch_name="$(branch)" -e commit_text="$(m)" -e repo_dir="$(repo_dir)" -e current_dir="$(current_dir)" -e linter="yes" -e push_dev="yes" -v

