<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">
        myClientName - название клиента.<br>
        my-сlient-name - красивое название клиента для создания путей, url
        <ol>
            <li>Скопировать папку <code>/clients/app-template</code> и переименовать в <code>/clients/my-сlient-name</code></li>
            <li>Добавить в массив <code>/common/config/clients-aliases.php</code> запись: <code> '@myClientName' => 'my-сlient-name',</code></li>
            <li>Изменить параметр “controllerNamespace” в файле <code>/clients/my-сlient-name/config/main.php</code></li>
            <li>Изменить namespace в контроллерах <code>/clients/my-сlient-name/controllers</code> моделях <code>/clients/my-сlient-name/models</code>  и файле <code>/clients/my-сlient-name/assets/AppAsset.php</code></li>
            <li>В файле  <code>/clients/my-сlient-name/views/layouts/main.php</code> заменить строку: <br>
                <code>use appTemplate\assets\AppAsset;</code><br>
                на строку: <br>
                <code>use myClientName\assets\AppAsset;</code>
            </li>
        </ol>
        
        
    </div>
</div>
