<?= $widgetName ?> = function() {
    
    var widget = this;
    
    this.render = function() {

    };
    this.bind_actions = function() {
        
    };
    this.init = function(){
        
    };
    this.bootstrap = function(code) {
        widget.code = code;
        widget.init();
		widget.render();
		widget.bind_actions();
		$(document).on('widgets:load', function(){
			widget.render();
		});
    };
}
yadroWidget.widgets['<?= $widgetCode ?>'] = new <?= $widgetName ?>();
yadroWidget.widgets['<?= $widgetCode ?>'].bootstrap('<?= $widgetCode ?>');

