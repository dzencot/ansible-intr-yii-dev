<?php

namespace appTemplate\models\db;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property string $queryString
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queryString'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'queryString' => 'Query String',
        ];
    }
}
