<?php
namespace testClient\controllers;

use Yii;


class WidgetController extends \common\controllers\WidgetController
{

    public function actionHideLetters()
    {
        return $this->renderPartial('view-name', [
            'widgetCode' => $this->getWidgetCode(), //'id приложения' . '-' . 'id контроллера'
            'widgetName' => 'MyWidgetName1234', // название объекта виджета в js, надо бы сделать динамичски
        ]);
    }
    
}
